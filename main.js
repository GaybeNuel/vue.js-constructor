new Vue ({
    el:'#constructor',    
    data :{
        currentcbitem:"cb-item1",
        cbitemstyle1:'cb-step--current',
        cbitemstyle2: '',
        cbitemstyle3: '',
        cbitemstyle4: '',
        currentimg:'',
        currentimgtext:'Не выбрано',
        currentframe:'',
        currentframetext:'Не выбрано',
        currentprinttext:'Не выбраны',
		titletext:'',
		nametext:'',
		datetext:'',
		activebtn1:'btn-font--active',
		activebtn2:'',
		activebtn3:'',
		currentfontstyle:'',
		checkmark1:'0',
		checkmark2:'0',
		checkmark3:'0',
		checkmark4:'0',
		checkmark5:'0',
		checkmark6:'0',
		checkmark7:'0',
		checkmark8:'0',
		checkmark9:'0',
        checkmark10:'0',
        markcounter:'0',
        gradtext:'background-image: linear-gradient(to right',
        currentgradient:'',
        colortext:'background-color:',
        colorscontainer:'',
        activefirstcolor:'',
        total:'550'
    },
    methods :{        
        currentimg1: function(){
            this.currentimg = "background-image:url('img/pics/01.jpg');",
            this.currentimgtext = "01"
        },
        currentimg2: function(){
            this.currentimg = "background-image:url('img/pics/02.jpg');",
            this.currentimgtext = "02"
        },
        currentimg3: function(){
            this.currentimg = "background-image:url('img/pics/03.jpg');",
            this.currentimgtext = "03"
        },
        currentimg4: function(){
            this.currentimg = "background-image:url('img/pics/04.jpg');",
            this.currentimgtext = "04"
        },
        currentimg5: function(){
            this.currentimg = "background-image:url('img/pics/05.jpg');",
            this.currentimgtext = "05"
        },
        currentimg6: function(){
            this.currentimg = "background-image:url('img/pics/06.jpg');",
            this.currentimgtext = "06"
        },
        currentimg7: function(){
            this.currentimg = "background-image:url('img/pics/07.jpg');",
            this.currentimgtext = "07"
        },
        currentimg8: function(){
            this.currentimg = "background-image:url('img/pics/08.jpg');",
            this.currentimgtext = "08"
        },
        currentimg9: function(){
            this.currentimg = "background-image:url('img/pics/09.jpg');",
            this.currentimgtext = "09"
        },
        currentimg10: function(){
            this.currentimg = "background-image:url('img/pics/10.jpg');",
            this.currentimgtext = "10"
        },
        currentimg11: function(){
            this.currentimg = "background-image:url('img/pics/11.jpg');",
            this.currentimgtext = "11"
        },
        currentframe1: function(){
            this.currentframe = "background-image:url('img/borders/rama-01.png')",
            this.currentframetext = "Багетная 1"
        },
        currentframe2: function(){
            this.currentframe = "background-image:url('img/borders/rama-02.png')",
            this.currentframetext = "Багетная 2"
        },
        currentframe3: function(){
            this.currentframe = "background-image:url('img/borders/rama-03.png')",
            this.currentframetext = "Багетная 3"
        },
        currentframe4: function(){
            this.currentframe = "background-image:url('img/borders/rama-04.png')",
            this.currentframetext = "Багетная 4"
        },
        currentframe5: function(){
            this.currentframe = "background-image:url('img/borders/rama-05.png')",
            this.currentframetext = "Багетная 5"
        },
        currentframe6: function(){
            this.currentframe = "background-image:url('img/borders/rama-06.png')",
            this.currentframetext = "Багетная 6"
        },
        currentframe7: function(){
            this.currentframe = "background-image:url('img/borders/rama-07.png')",
            this.currentframetext = "Багетная 7"
        },
        currentframe8: function(){
            this.currentframe = "background-image:url('img/borders/rama-08.png')",
            this.currentframetext = "Багетная 8"
        },
        currentframe9: function(){
            this.currentframe = "background-image:url('img/borders/rama-09.png')",
            this.currentframetext = "Багетная 9"
        },
        currentframe10: function(){
            this.currentframe = "background-image:url('img/borders/rama-10.png')",
            this.currentframetext = "Багетная 10"
        },
        currentframe11: function(){
            this.currentframe = "background-image:url('img/borders/rama-11.png')",
            this.currentframetext = "Багетная 11"
        },
        currentframe12: function(){
            this.currentframe = "background-image:url('img/borders/rama-12.png')",
            this.currentframetext = "Багетная 12"
        },
        currentframe13: function(){
            this.currentframe = "background-image:url('img/borders/rama-13.png')",
            this.currentframetext = "Багетная 13"
        },
        currentframe14: function(){
            this.currentframe = "background-image:url('img/borders/rama-14.png')",
            this.currentframetext = "Багетная 14"
        },
        currentframe15: function(){
            this.currentframe = "background-image:url('img/borders/rama-15.png')",
            this.currentframetext = "Багетная 15"
        },
		currentfont1: function(){
			this.activebtn1="btn-font--active",
			this.activebtn2="",
			this.activebtn3="",
			this.currentfontstyle="font-family: ariston;"
		},
		currentfont2: function(){
			this.activebtn2="btn-font--active",
			this.activebtn1="",
			this.activebtn3="",
			this.currentfontstyle="font-family: davinci;"
		},
		currentfont3: function(){
			this.activebtn3="btn-font--active",
			this.activebtn1="",
			this.activebtn2="",
			this.currentfontstyle="font-family: brody;"
        },
        bindcolor1: function(){            
            for(;this.checkmark1 == 0;){
                this.checkmark1++;
                this.markcounter++;                
                this.colorscontainer += ",blue";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark1 == 1;){
                this.checkmark1--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",blue","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor2: function(){            
            for(;this.checkmark2 == 0;){
                this.checkmark2++;
                this.markcounter++;                
                this.colorscontainer += ",lime";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark2 == 1;){
                this.checkmark2--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",lime","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor3: function(){            
            for(;this.checkmark3 == 0;){
                this.checkmark3++;
                this.markcounter++;                
                this.colorscontainer += ",mediumorchid";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark3 == 1;){
                this.checkmark3--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",mediumorchid","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor4: function(){            
            for(;this.checkmark4 == 0;){
                this.checkmark4++;
                this.markcounter++;                
                this.colorscontainer += ",gold";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";  
                    break;                    
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark4 == 1;){
                this.checkmark4--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",gold","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor5: function(){            
            for(;this.checkmark5 == 0;){
                this.checkmark5++;
                this.markcounter++;                
                this.colorscontainer += ",deeppink";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark5 == 1;){
                this.checkmark5--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",deeppink","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";  
                    break;                    
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor6: function(){            
            for(;this.checkmark6 == 0;){
                this.checkmark6++;
                this.markcounter++;                
                this.colorscontainer += ",darkorange";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark6 == 1;){
                this.checkmark6--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",darkorange","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        bindcolor7: function(){            
            for(;this.checkmark7 == 0;){
                this.checkmark7++;
                this.markcounter++;                
                this.colorscontainer += ",forestgreen";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark7 == 1;){
                this.checkmark7--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",forestgreen","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },        
        bindcolor10: function(){            
            for(;this.checkmark10 == 0;){
                this.checkmark10++;
                this.markcounter++;                
                this.colorscontainer += ",darksalmon";                
                this.currentgradient = this.gradtext + this.colorscontainer + ");";  
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                                                             
                    break;                                       
                }; 
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                                            
                    break;
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"                                                                        
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;"; 
                    break;                    
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                };
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                             
                return false;

            };
            for(;this.checkmark10 == 1;){
                this.checkmark10--;
                this.markcounter--;                
                this.colorscontainer = this.colorscontainer.replace(",darksalmon","");
                this.currentgradient = this.gradtext + this.colorscontainer + ");";
                for(;this.markcounter <= 1;){
                    this.currentgradient +="display:none;";                                       
                    break;
                };
                for(;this.markcounter >= 2;){
                    this.currentgradient = this.currentgradient.replace("display:none;","");                                       
                    break;                    
                };
                for(;this.markcounter == 1;){
                    this.activefirstcolor = this.colortext + this.currentgradient;
                    this.activefirstcolor = this.activefirstcolor.replace("background-image: linear-gradient(to right,",""); 
                    this.activefirstcolor = this.activefirstcolor.replace(");display:none;",""); 
                    this.activefirstcolor += ";"
                    break;
                }
                for(;this.markcounter == 0;){
                    this.activefirstcolor = "display:none;";
                    break;                      
                };
                for(;this.markcounter >= 2;){
                    this.activefirstcolor = "display:none;"; 
                    break;                     
                }; 
                for(;this.markcounter >= 3;){
                    this.total = 2050;
                    break;                
                };
                for(;this.markcounter <= 2;){
                    this.total = 550;
                    break;                
                };                                              
                return false;
            };
        },
        nextstep: function(){
            for(;this.currentcbitem == "cb-item1";){
                this.currentcbitem = "cb-item2";
                this.cbitemstyle2 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle3 = "";
                this.cbitemstyle4 = "";
                return false;
            };
            for(;this.currentcbitem == "cb-item2";){
                this.currentcbitem = "cb-item3";
                this.cbitemstyle3 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle2 = "";
                this.cbitemstyle4 = "";
                return false;
            };
            for(;this.currentcbitem == "cb-item3";){
                this.currentcbitem = "cb-item4";
                this.cbitemstyle4 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle2 = "";
                this.cbitemstyle3 = "";
				return false;
            };
        }
		/*placemark1: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark1 == 0;){                
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("blue);");
                    this.checkmark1++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",blue);");
                    this.checkmark1++;
                    this.markcounter++; 
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                   
                    return false;                
                };                
            };
            for(;this.checkmark1 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",blue","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", blue","");                
                this.markcounter--;
                this.checkmark1--;
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                                
                return false;
            };
                 
        },
        placemark2: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark2 == 0;){
                for(;this.markcounter == 0;){                                        
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("lime);");
                    this.checkmark2++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",lime);");
                    this.checkmark2++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                    
                    return false;                
                };                
            };
            for(;this.checkmark2 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",lime","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", lime","");                                
                this.markcounter--;
                this.checkmark2--; 
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                               
                return false;
            };            
        },
        placemark3: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark3 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("MediumOrchid);");
                    this.checkmark3++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",MediumOrchid);");
                    this.checkmark3++;
                    this.markcounter++;                    
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;                
                };                
            };
            for(;this.checkmark3 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",MediumOrchid","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", MediumOrchid","");                
                this.markcounter--;
                this.checkmark3--; 
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                               
                return false;
            };                  
        },
        placemark4: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark4 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("Gold);");
                    this.checkmark4++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    console.log(this.currentfirstcolor);                
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",Gold);");
                    this.checkmark4++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                    
                    return false;                
                };                
            };
            for(;this.checkmark4 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",Gold","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", Gold","");                
                this.markcounter--;
                this.checkmark4--; 
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                               
                return false;
            };                    
        },
        placemark5: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark5 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("DeepPink);");
                    this.checkmark5++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",DeepPink);");
                    this.checkmark5++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                    
                    return false;                
                };                
            };
            for(;this.checkmark5 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",DeepPink","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", DeepPink","");                
                this.markcounter--;
                this.checkmark5--; 
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                               
                return false;
            };                   
        },
        placemark6: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark6 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("DarkOrange);");
                    this.checkmark6++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",DarkOrange);");
                    this.checkmark6++;
                    this.markcounter++; 
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                   
                    return false;                
                };                
            };
            for(;this.checkmark6 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",DarkOrange","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", DarkOrange","");                
                this.markcounter--;
                this.checkmark6--;
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                                
                return false;
            };                    
        },
        placemark7: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark7 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("ForestGreen);");
                    this.checkmark7++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",ForestGreen);");
                    this.checkmark7++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                    
                    return false;                
                };                
            };
            for(;this.checkmark7 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",ForestGreen","");
                this.currentmarkstyle = this.currentmarkstyle.replace(", ForestGreen","");                
                this.markcounter--;
                this.checkmark7--;
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                                
                return false;
            };
                    
        },        
        placemark10: function(){
            for(;this.markcounter >= 3;){
                this.total = 2050;
                break;                
            };
            for(;this.markcounter <= 2;){
                this.total = 550;
                break;                
            };
            for(;this.checkmark10 == 0;){
                for(;this.markcounter == 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat("DarkSalmon);");
                    this.checkmark10++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);
                    return false;
                };
                for(;this.markcounter > 0;){                    
                    this.currentmarkstyle = this.currentmarkstyle.replace(");","");
                    this.currentmarkstyle = this.currentmarkstyle.concat(",DarkSalmon);");
                    this.checkmark10++;
                    this.markcounter++;
                    this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                    this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                    
                    return false;                
                };                
            };
            for(;this.checkmark10 == 1;){                
                this.currentmarkstyle = this.currentmarkstyle.replace(",DarkSalmon",""); 
                this.currentmarkstyle = this.currentmarkstyle.replace(", DarkSalmon","");               
                this.markcounter--;
                this.checkmark10--;
                this.forcounting = this.currentmarkstyle.slice(43).replace(");",";");
                this.currentfirstcolor = this.currentfirstcolor.concat(this.forcounting);                                
                return false;
            };                   
        },        
        nextstep: function(){
            for(;this.currentcbitem == "cb-item1";){
                this.currentcbitem = "cb-item2";
                this.cbitemstyle2 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle3 = "";
                this.cbitemstyle4 = "";
                return false;
            };
            for(;this.currentcbitem == "cb-item2";){
                this.currentcbitem = "cb-item3";
                this.cbitemstyle3 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle2 = "";
                this.cbitemstyle4 = "";
                return false;
            };
            for(;this.currentcbitem == "cb-item3";){
                this.currentcbitem = "cb-item4";
                this.cbitemstyle4 = "cb-step--current";
                this.cbitemstyle1 = "";
                this.cbitemstyle2 = "";
                this.cbitemstyle3 = "";
				return false;
            };
        }*/                    
    }
});
